# 黑马旅游网

## 登录

请求路径：/user/login

请求方法：POST

请求参数：

| 参数名   | 参数类型 | 参数意义 |
| -------- | -------- | -------- |
| username | String   | 用户名   |
| password | String   | 密码     |
| check    | String   | 验证码   |

返回结果：

```json
{
	"flag":true,
    "data":{
        "uid":1,
        "usename":"yx",
        "name":""
    }
}
```



