```
instanceof 实例
raw 原型
property 属性
symbol 标志
description 描述
reflect 反射
define 定义
type of 类型
pattern 模式
exec 执行
source 源
multiline 多线，多行
igonreCase 不区分大小写
global 全局的
sticky 粘附
insensitive 不敏感
case-insensitive 不区分大小写
result 结果
parse 解析，理解
prev 上一个
reduce 减量化
foreach 为每一个
filter 过滤
item 项目，项
splice 拼接
slice 切片
concat 合并多个字符串或数组
sort 排序
push 推入
shift 上档
inclub 包含
repeat 重复
pad 填充
iterator 迭代器
LowerCase 小写
UpperCase 大写
regexp 正则表达式
search 搜索，查找
replace 替代

```

